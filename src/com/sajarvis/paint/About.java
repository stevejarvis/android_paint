package com.sajarvis.paint;

import android.app.Activity;
import android.os.Bundle;
import com.sajarvis.fingerpaint.R;

public class About extends Activity{	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
	}
}
